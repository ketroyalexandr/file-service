package config

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"github.com/ketroyalexandr/uploader-go/internal/testutil"
)

const configPath = "./test/config_test.yaml"

type configSuite struct {
	testutil.Suite
	manager *Manager
}

func (s *configSuite) SetupTest() {
	var err error
	s.manager, err = NewManager(configPath)
	s.Require().NoError(err, "Can't initialize manager")
}

func (s *configSuite) TestListen() {
	s.Equal("0.0.0.0:8080", s.manager.Listen())
}

func (s *configSuite) TestShutdownTimeout() {
	s.Equal(15*time.Second, s.manager.ShutdownTimeout())
}

func (s *configSuite) TestConnectorTimeout() {
	s.Equal(2*time.Second, s.manager.ConnectorTimeout())
}

func (s *configSuite) TestRedisHost() {
	s.Equal("localhost:6379", s.manager.RedisHost())
}

func (s *configSuite) TestDiskDir() {
	s.Equal("../../../pkg/fileservice/disk/", s.manager.DiskDir())
}

func TestContent(t *testing.T) {
	suite.Run(t, new(configSuite))
}
