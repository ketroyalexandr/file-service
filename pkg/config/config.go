package config

import (
	"time"

	"github.com/spf13/viper"
)

const (
	listen           = "listen"
	shutdownTimeout  = "shutdown_timeout"
	connectorTimeout = "connector_timeout"
	redisHost        = "redis.host"
	redisPassword    = "redis.host"
	diskDir          = "disk_dir"
)

type Manager struct {
	Path      string
	viperCnfg *viper.Viper
}

func NewManager(configPath string) (*Manager, error) {
	configManager := &Manager{Path: configPath, viperCnfg: viper.New()}
	configManager.setDefaults()

	err := configManager.readFromFile(configPath)
	if err != nil {
		return nil, err
	}

	return configManager, err
}

func (m Manager) setDefaults() {
	m.viperCnfg.SetDefault(listen, ":8080")
	m.viperCnfg.SetDefault(redisHost, "localhost")
	m.viperCnfg.SetDefault(shutdownTimeout, 15*time.Second)
}

func (m *Manager) readFromFile(filename string) error {
	m.viperCnfg.SetConfigFile(filename)
	return m.viperCnfg.ReadInConfig()
}

// Listen returns address services should run on (e.g. localhost:8080)
func (m *Manager) Listen() string {
	return m.viperCnfg.GetString(listen)
}

// ShutdownTimeout returns maximum wait time for HTTP server to gracefully shutdown
func (m *Manager) ShutdownTimeout() time.Duration {
	return m.viperCnfg.GetDuration(shutdownTimeout)
}

// ConnectorTimeout returns maximum wait time for HTTP request
func (m *Manager) ConnectorTimeout() time.Duration {
	return m.viperCnfg.GetDuration(connectorTimeout)
}

// RedisHost returns redis host
func (m *Manager) RedisHost() string {
	return m.viperCnfg.GetString(redisHost)
}

// RedisPassword returns redis password
func (m *Manager) RedisPassword() string {
	return m.viperCnfg.GetString(redisPassword)
}

// DiskDir returns file store directory path
func (m *Manager) DiskDir() string {
	return m.viperCnfg.GetString(diskDir)
}
