package controller

import "github.com/stretchr/testify/mock"

type fileServerMock struct {
	mock.Mock
}

func (m *fileServerMock) Store(data []byte) error {
	args := m.Called(data)
	return args.Error(0)
}

func (m *fileServerMock) GetFile(file string) ([]byte, error) {
	args := m.Called(file)
	return args.Get(0).([]byte), args.Error(1)
}
