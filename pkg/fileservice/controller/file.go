package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/ketroyalexandr/uploader-go/pkg/fileservice/domain"
)

type IFile interface {
	Store([]byte) error
	GetFile(string) ([]byte, error)
}

type fileController struct {
	service domain.IFile
}

func New(fileService IFile) *fileController {
	return &fileController{
		service: fileService,
	}
}

func (c *fileController) Save(ctx *gin.Context) {
	data, err := ctx.GetRawData()
	if err != nil || len(data) == 0 {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "something went wrong"})
		return
	}
	err = c.service.Store(data)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "Failed on saving a file"})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "File uploaded successfully"})
}

func (c *fileController) Extract(ctx *gin.Context) {
	file := ctx.Param("file")
	data, err := c.service.GetFile(file)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "Failed on extracting a file part" + err.Error()})
		return
	}

	ctx.Header("Content-Disposition", "attachment; filename="+file)
	ctx.Data(http.StatusOK, "application/octet-stream", data)
}
