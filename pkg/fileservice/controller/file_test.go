package controller

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/suite"

	"github.com/ketroyalexandr/uploader-go/internal/testutil"
)

type fileRouteSuit struct {
	testutil.Suite
	fs         *fileServerMock
	controller *fileController
}

func (s *fileRouteSuit) SetupTest() {
	s.fs = new(fileServerMock)
	s.fs.Test(s.T())
	s.controller = New(s.fs)
}

func (s *fileRouteSuit) Test_Save_Success() {
	requestBody := []byte(`some_file_info`)
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest(http.MethodPut, "http://localhost:8080/save", nil)
	ctx.Request.Body = io.NopCloser(bytes.NewReader(requestBody))
	s.fs.On("Store", requestBody).
		Return(nil).
		Once()

	s.controller.Save(ctx)

	s.Equal(http.StatusOK, w.Code)
}

func (s *fileRouteSuit) Test_Save_BadRequest() {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest(http.MethodPut, "http://localhost:8080/save", nil)

	s.controller.Save(ctx)

	s.Equal(http.StatusBadRequest, w.Code)
}

func (s *fileRouteSuit) Test_FileStore_Failed() {
	requestBody := []byte(`some_file_info`)
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = httptest.NewRequest(http.MethodPut, "http://localhost:8080/save", nil)
	ctx.Request.Body = io.NopCloser(bytes.NewReader(requestBody))
	s.fs.On("Store", requestBody).
		Return(errors.New("failed")).
		Once()

	s.controller.Save(ctx)

	s.Equal(http.StatusInternalServerError, w.Code)
}

func (s *fileRouteSuit) Extract() {

}

func (s *fileRouteSuit) TearDownTest() {
	s.fs.AssertExpectations(s.T())
}

func TestRouter(t *testing.T) {
	suite.Run(t, &fileRouteSuit{})
}
