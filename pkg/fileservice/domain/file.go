package domain

import (
	"errors"
	"os"
	"strings"

	"github.com/ketroyalexandr/uploader-go/internal"
	"github.com/ketroyalexandr/uploader-go/pkg/config"
)

type IFile interface {
	Store([]byte) error
	GetFile(string) ([]byte, error)
}

type fileServer struct {
	config *config.Manager
}

func New(cnf *config.Manager) *fileServer {
	return &fileServer{
		config: cnf,
	}
}

func (f *fileServer) Store(data []byte) error {
	dir, err := internal.DiscoverMachineIP()
	if err != nil {
		return err
	}

	storageFolderPath := f.config.DiskDir() + dir
	_, err = os.Stat(storageFolderPath)
	if os.IsNotExist(err) {
		internal.CreateDirectory(storageFolderPath)
	}
	metadata := internal.ParseMetadata(data)
	if metadata == nil {
		return errors.New("no metadata. broken file part")
	}

	err = os.WriteFile(storageFolderPath+"/"+metadata.TmpName, data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (f *fileServer) GetFile(file string) ([]byte, error) {
	dir, err := internal.DiscoverMachineIP()
	if err != nil {
		return nil, err
	}

	fileInfo := strings.Split(file, ".")
	storageFolderPath := f.config.DiskDir() + dir
	tmpFileName, err := internal.EncodedFileName(fileInfo[0], fileInfo[1])
	if err != nil {
		return nil, err
	}

	return os.ReadFile(storageFolderPath + "/" + tmpFileName)
}
