package domain

import (
	"encoding/binary"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"

	"github.com/ketroyalexandr/uploader-go/internal/testutil"
	"github.com/ketroyalexandr/uploader-go/pkg/config"
)

const configPath = "../../config/test/config_test.yaml"

type fileServiceSuite struct {
	testutil.Suite
	fs IFile
}

func (s *fileServiceSuite) SetupTest() {
	cnf, _ := config.NewManager(configPath)
	s.fs = New(cnf)
}

func (s *fileServiceSuite) TestStore() {
	metadata := prepareMetadata("image", "jpg", 1)
	b := make([]byte, 0, len(metadata))
	b = append(b, metadata...)

	err := s.fs.Store(b)

	s.Require().NoError(err)
}

func (s *fileServiceSuite) TestGetFile() {
	metadata := prepareMetadata("image", "jpg", 1)
	b := make([]byte, 0, len(metadata))
	b = append(b, metadata...)
	err := s.fs.Store(b)
	s.Require().NoError(err)

	data, err := s.fs.GetFile("image.jpg")

	s.Require().NoError(err)
	s.Equal(b, data)
}

func prepareMetadata(filename, ext string, order int) []byte {
	metadataStr := []string{filename, ext, strconv.Itoa(order)}
	payload := strings.Join(metadataStr, "|")
	metadataPayload := make([]byte, 0, len(payload))
	metadataPayload = append(metadataPayload, []byte(payload)...)
	metadata := make([]byte, len(metadataPayload)+2)
	copy(metadata[2:], metadataPayload)
	mdLen := uint16(len(metadataPayload))
	binary.BigEndian.PutUint16(metadata, mdLen)

	return metadata
}

func TestContent(t *testing.T) {
	suite.Run(t, new(fileServiceSuite))
}
