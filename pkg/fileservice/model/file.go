package model

// Metadata describe file metadata
type Metadata struct {
	End      uint16
	FileName string
	Ext      string
	TmpName  string
	Order    int
}
