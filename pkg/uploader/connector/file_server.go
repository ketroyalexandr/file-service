package connector

import (
	"bytes"
	"context"
	"fmt"
	"net/http"

	"github.com/ketroyalexandr/uploader-go/pkg/config"
)

const (
	saveFileURL    = "http://%s:8080/save"
	extractFileURL = "http://%s:8080/extract/%s"
)

type IConnector interface {
	Send(context.Context, []byte, string) (*http.Response, error)
	GetFile(string, string) (*http.Response, error)
}

type fileServerConnector struct {
	client *http.Client
}

func New(cnf *config.Manager) *fileServerConnector {
	return &fileServerConnector{
		client: &http.Client{
			Timeout:   cnf.ConnectorTimeout(),
			Transport: &http.Transport{},
		},
	}
}

func (fs *fileServerConnector) Send(ctx context.Context, body []byte, url string) (*http.Response, error) {
	reqURL := fmt.Sprintf(saveFileURL, url)
	req, err := http.NewRequest(http.MethodPut, reqURL, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/octet-stream")
	req = req.WithContext(ctx)

	return fs.client.Do(req)
}

func (fs *fileServerConnector) GetFile(file, url string) (*http.Response, error) {
	reqURL := fmt.Sprintf(extractFileURL, url, file)
	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	if err != nil {
		return nil, err
	}

	return fs.client.Do(req)
}
