package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"net/http"

	"github.com/ketroyalexandr/uploader-go/pkg/config"
	"github.com/ketroyalexandr/uploader-go/pkg/uploader/domain"
)

type IUploader interface {
	Upload(*gin.Context)
	FileServices(*gin.Context)
}

type uploaderController struct {
	cnf     *config.Manager
	service domain.IUploaderDomain
}

func NewUploader(config *config.Manager, service domain.IUploaderDomain) *uploaderController {
	return &uploaderController{
		cnf:     config,
		service: service,
	}
}

func (c *uploaderController) Upload(ctx *gin.Context) {
	file, err := ctx.FormFile("file")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err})
		return
	}

	err = c.service.DistributeFile(ctx, file)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf(" - '%s' uploaded!", file.Filename)})
}

func (c *uploaderController) FileServices(ctx *gin.Context) {
	response, err := c.service.GetFileServices()
	if err != nil && err != redis.Nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"response": response})
}

func (c *uploaderController) Download(ctx *gin.Context) {
	fileName := ctx.Param("file")
	data, err := c.service.AssembleDistributedFile(fileName)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}

	ctx.Header("Content-Disposition", "attachment; filename="+fileName)
	ctx.Data(http.StatusOK, "application/octet-stream", data)
}
