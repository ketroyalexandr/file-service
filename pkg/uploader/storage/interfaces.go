package storage

type IRedisStorage interface {
	Ping() (string, error)
	Purge() error
	Set(string, interface{}) error
	GetBytes(key string) ([]byte, error)
}
