package storage

import (
	"os"

	"github.com/go-redis/redis"

	"github.com/ketroyalexandr/uploader-go/pkg/config"
)

type redisStorage struct {
	client *redis.Client
}

func NewRedisStorage(cnf *config.Manager) (*redisStorage, error) {
	redisHost := cnf.RedisHost()
	if os.Getenv("REDIS_LOCAL") == "1" {
		redisHost = "localhost:6379"
	}
	redisClient := redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: "",
		DB:       0,
	})

	err := redisClient.Ping().Err()
	if err != nil {
		return nil, err
	}

	return &redisStorage{
		client: redisClient,
	}, nil
}

func (r *redisStorage) Ping() (string, error) {
	ping := r.client.Ping()
	return ping.String(), ping.Err()
}

func (r *redisStorage) Purge() error {
	// TODO: must be implemented
	return nil
}

func (r *redisStorage) Set(key string, data interface{}) error {
	return r.client.Set(key, data, 0).Err()
}

func (r *redisStorage) GetBytes(key string) ([]byte, error) {
	return r.client.Get(key).Bytes()
}
