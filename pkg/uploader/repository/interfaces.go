package repository

type IStorage interface {
	Set(string, interface{}) error
	GetBytes(key string) ([]byte, error)
}
