package repository

import (
	"encoding/json"

	"github.com/ketroyalexandr/uploader-go/pkg/uploader/model"
)

const FileServiceList = "file_services"

type IUploaderRepository interface {
	AddFileService([]byte) error
	GetFileServices() (model.FileService, error)
}

type uploaderRepository struct {
	storage IStorage
}

func New(storage IStorage) *uploaderRepository {
	return &uploaderRepository{
		storage: storage,
	}
}

func (r *uploaderRepository) AddFileService(data []byte) error {
	return r.storage.Set(FileServiceList, data)
}

func (r *uploaderRepository) GetFileServices() (model.FileService, error) {
	data, err := r.storage.GetBytes(FileServiceList)
	if err != nil {
		return model.FileService{}, err
	}

	var services model.FileService
	err = json.Unmarshal(data, &services)
	if err != nil {
		return model.FileService{}, err
	}

	return services, nil
}
