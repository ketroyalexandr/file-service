package domain

import (
	"context"
	"github.com/ketroyalexandr/uploader-go/pkg/uploader/model"
	"net/http"
)

type IUploaderRepository interface {
	AddFileService([]byte) error
	GetFileServices() (model.FileService, error)
}

type IConnector interface {
	Send(context.Context, []byte, string) (*http.Response, error)
	GetFile(string, string) (*http.Response, error)
}
