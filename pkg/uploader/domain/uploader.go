package domain

import (
	"context"
	"encoding/binary"
	"encoding/json"
	"io"
	"log"
	"mime/multipart"
	"strconv"
	"strings"
	"sync"

	"github.com/go-redis/redis"

	"github.com/ketroyalexandr/uploader-go/internal"
	"github.com/ketroyalexandr/uploader-go/pkg/uploader/model"
)

type IUploaderDomain interface {
	RegisterFileService(string) error
	GetFileServices() (model.FileService, error)
	DistributeFile(context.Context, *multipart.FileHeader) error
	AssembleDistributedFile(string) ([]byte, error)
}

type uploaderService struct {
	repository IUploaderRepository
	connector  IConnector
}

func New(repo IUploaderRepository, fsConnector IConnector) *uploaderService {
	return &uploaderService{
		repository: repo,
		connector:  fsConnector,
	}
}

func (u *uploaderService) RegisterFileService(ip string) error {
	services, err := u.repository.GetFileServices()
	if err != nil && err != redis.Nil {
		return err
	}

	services.ServiceList = append(services.ServiceList, ip)
	data, err := json.Marshal(services)
	if err != nil {
		return err
	}

	return u.repository.AddFileService(data)
}

func (u *uploaderService) GetFileServices() (model.FileService, error) {
	return u.repository.GetFileServices()
}

func (u *uploaderService) DistributeFile(ctx context.Context, mpf *multipart.FileHeader) error {
	file, err := mpf.Open()
	if err != nil {
		return err
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal("cannot close file")
		}
	}()

	data, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	fs, err := u.repository.GetFileServices()
	if err != nil {
		return err
	}

	fsCount := len(fs.ServiceList)
	partSize := mpf.Size / int64(fsCount)
	var wg sync.WaitGroup
	wg.Add(fsCount)
	for i, addr := range fs.ServiceList {
		start := int64(i) * partSize
		end := start + partSize
		if i == fsCount-1 {
			end = mpf.Size
		}
		part := data[start:end]
		metadata := u.prepareMetadata(mpf, i)
		body := make([]byte, 0, len(metadata)+len(part))
		body = append(body, metadata...)
		body = append(body, part...)
		go func(ctx context.Context, b []byte, url string) {
			wg.Done()
			_, err = u.connector.Send(ctx, b, url)
			if err != nil {
				log.Fatal("connector send error:", err)
			}
		}(ctx, body, addr)
	}
	wg.Wait()

	return err
}

func (u *uploaderService) AssembleDistributedFile(file string) ([]byte, error) {
	fs, err := u.repository.GetFileServices()
	if err != nil {
		return nil, err
	}

	var wg sync.WaitGroup
	fsCount := len(fs.ServiceList)
	dataChannel := make(chan []byte, fsCount)
	wg.Add(fsCount)
	for _, addr := range fs.ServiceList {
		go func(url string) {
			defer wg.Done()
			d, err := u.connector.GetFile(file, url)
			if err != nil {
				log.Fatal("failed on get file error:", err)
			}
			defer d.Body.Close()

			rb, err := io.ReadAll(d.Body)
			if err != nil {
				log.Fatal("failed on reading response body:", err)
			}
			dataChannel <- rb
		}(addr)
	}
	wg.Wait()
	close(dataChannel)

	filePartsMap := make(map[int][]byte, len(dataChannel))
	for part := range dataChannel {
		metadata := internal.ParseMetadata(part)
		filePartsMap[metadata.Order] = part[metadata.End:]
	}
	data := make([]byte, 0)
	for i := 0; i < fsCount; i++ {
		data = append(data, filePartsMap[i]...)
	}

	return data, err
}

func (u *uploaderService) prepareMetadata(mpf *multipart.FileHeader, order int) []byte {
	fileInfo := strings.Split(mpf.Filename, ".")
	metadataStr := []string{fileInfo[0], fileInfo[1], strconv.Itoa(order)}
	payload := strings.Join(metadataStr, "|")
	metadataPayload := make([]byte, 0, len(payload))
	metadataPayload = append(metadataPayload, []byte(payload)...)
	metadata := make([]byte, len(metadataPayload)+2)
	copy(metadata[2:], metadataPayload)
	binary.BigEndian.PutUint16(metadata, uint16(len(metadataPayload)))

	return metadata
}
