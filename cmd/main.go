package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"

	"github.com/ketroyalexandr/uploader-go/app"
	"github.com/ketroyalexandr/uploader-go/internal"
	cfg "github.com/ketroyalexandr/uploader-go/pkg/config"
	fController "github.com/ketroyalexandr/uploader-go/pkg/fileservice/controller"
	fService "github.com/ketroyalexandr/uploader-go/pkg/fileservice/domain"
	"github.com/ketroyalexandr/uploader-go/pkg/uploader/connector"
	uController "github.com/ketroyalexandr/uploader-go/pkg/uploader/controller"
	uService "github.com/ketroyalexandr/uploader-go/pkg/uploader/domain"
	uRepository "github.com/ketroyalexandr/uploader-go/pkg/uploader/repository"
	"github.com/ketroyalexandr/uploader-go/pkg/uploader/storage"
)

func main() {
	cnf, err := cfg.NewManager("./pkg/config/config.yaml")
	if err != nil {
		log.Fatalf("could not initialize config: %s", err.Error())
	}

	s, err := storage.NewRedisStorage(cnf)
	if err != nil {
		log.Fatalf("failed to connect to redis: %s", err.Error())
	}

	application := app.Init(cnf, s)
	fsConnector := connector.New(cnf)
	uplRepository := uRepository.New(s)
	uplService := uService.New(uplRepository, fsConnector)
	uplController := uController.NewUploader(cnf, uplService)
	fileService := fService.New(cnf)
	fileController := fController.New(fileService)

	r := gin.Default()
	r.GET("/_health", health)
	r.GET("/file-services", uplController.FileServices)
	r.PUT("/upload", uplController.Upload)
	r.GET("/download/:file", uplController.Download)
	r.PUT("/save", fileController.Save)
	r.GET("/extract/:file", fileController.Extract)

	isFileService := os.Getenv("IS_FILE_SERVICE") == "true"
	if isFileService {
		autoDiscoverFileService(uplService)
	}

	go func() {
		err = application.Run(r)
		if err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	ListenForSignals([]os.Signal{syscall.SIGINT, syscall.SIGTERM})
	application.Shutdown()
}

func ListenForSignals(signals []os.Signal) {
	ctx, stop := signal.NotifyContext(context.Background(), signals...)
	defer stop()
	<-ctx.Done()
}

func health(ctx *gin.Context) {
	ctx.JSON(200, "I'm working!")
}

func autoDiscoverFileService(uplService uService.IUploaderDomain) {
	ip, err := internal.DiscoverMachineIP()
	if err != nil {
		log.Printf("cannot discover ip: %s", err)
		return
	}

	err = uplService.RegisterFileService(ip)
	if err != nil {
		log.Printf("cannot register service with ip: %s", ip)
		return
	}
	log.Printf("registered machine with ip: %s", ip)
}
