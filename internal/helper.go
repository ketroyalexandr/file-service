package internal

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"

	"go.uber.org/zap"

	"github.com/ketroyalexandr/uploader-go/pkg/fileservice/model"
)

// DiscoverMachineIP discovering container ip
func DiscoverMachineIP() (string, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		fmt.Println("dial error", zap.Error(err))
		return "", err
	}

	err = conn.Close()
	if err != nil {
		return "", err
	}

	ipAddr := strings.Split(conn.LocalAddr().(*net.UDPAddr).String(), ":")
	return ipAddr[0], nil
}

// CreateDirectory create new directory
func CreateDirectory(subFolderPath string) {
	err := os.Mkdir(subFolderPath, 0777)
	if err != nil {
		log.Fatal("cannot make storage folder:", err)
	}
	fmt.Println("folder created")
}

// EncodedFileName return md5 encoded file name
func EncodedFileName(filename, ext string) (string, error) {
	fullFileName := filename + "." + ext
	h := md5.New()
	_, err := io.WriteString(h, fullFileName)
	if err != nil {
		return "", err
	}

	hashBytes := h.Sum(nil)
	fileEncodedName := fmt.Sprintf("%x", hashBytes) + ".part"
	return fileEncodedName, nil
}

// ParseMetadata return metadata struct
func ParseMetadata(data []byte) *model.Metadata {
	metadataEnd := binary.BigEndian.Uint16(data[:2]) + 2
	payload := strings.Split(string(data[2:metadataEnd]), "|")
	if len(payload) == 0 {
		return nil
	}

	partOrder, err := strconv.Atoi(payload[2])
	if err != nil {
		log.Fatal("Cannot parse metadata")
	}
	metadata := &model.Metadata{
		End:      metadataEnd,
		FileName: payload[0],
		Ext:      payload[1],
		Order:    partOrder,
	}
	tmpName, err := EncodedFileName(metadata.FileName, metadata.Ext)
	if err != nil {
		log.Fatalln("failed on writing string")
	}
	metadata.TmpName = tmpName

	return metadata
}
