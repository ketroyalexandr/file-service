package testutil

import (
	"net/http"
	"net/http/httputil"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

// Suite is a test suite that has helper functions
type Suite struct {
	suite.Suite
}

// Run runs a sub-test inside a suite test and
// correctly sets and resets *testing.T
func (s *Suite) Run(name string, test func()) {
	s.T().Run(name, func(t *testing.T) {
		defer s.SetT(s.T())
		s.SetT(t)
		test()
	})
}

// SafeClient is a wrapper of http.Client
// Fails if client.Do returns an error
type SafeClient struct {
	*testing.T
	*http.Client
}

// Do sends an HTTP request and returns an HTTP response
// Fails if an error is returned
func (s *SafeClient) Do(req *http.Request) *http.Response {
	s.T.Helper()
	reqDump, err := httputil.DumpRequestOut(req, true)
	require.NoError(s.T, err, "could not dump request")

	resp, err := s.Client.Do(req)
	require.NoErrorf(s.T, err, "failed to execute http request.\nRequest:\n%s", string(reqDump))

	respDump, err := httputil.DumpResponse(resp, true)
	require.NoErrorf(s.T, err, "could not dump response")

	s.T.Logf("successfully performed http request.\nRequest:\n%s\nResponse:\n%s", string(reqDump), string(respDump))
	return resp
}
