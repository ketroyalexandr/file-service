package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/ketroyalexandr/uploader-go/pkg/config"
)

type IStorage interface {
	Ping() (string, error)
}

type Application struct {
	storage    IStorage
	httpServer *http.Server
	config     *config.Manager
}

func Init(cnf *config.Manager, storage IStorage) *Application {
	return &Application{
		storage: storage,
		config:  cnf,
	}
}

func (fs *Application) Run(router *gin.Engine) error {
	fs.httpServer = &http.Server{
		Addr:    fs.config.Listen(),
		Handler: router,
	}
	return fs.httpServer.ListenAndServe()
}

func (fs *Application) Shutdown() {
	ch := make(chan struct{})
	go func() {
		fs.stop()
		close(ch)
	}()
	select {
	case <-ch:
		log.Println("application was shut down")
	case <-time.After(fs.config.ShutdownTimeout()):
		log.Fatalf("could not shut down in %s", fs.config.ShutdownTimeout().String())
	}
	os.Exit(0)
}

func (fs *Application) stop() {
	ctx, cancel := context.WithTimeout(context.Background(), fs.config.ShutdownTimeout())
	defer cancel()
	if err := fs.httpServer.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}
	fmt.Println("all data will be cleaned up")
}
