//go:build e2e
// +build e2e

package e2e

import (
	"bytes"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/ketroyalexandr/uploader-go/internal/testutil"
)

func TestUploadFile(t *testing.T) {
	file, err := os.Open("../../internal/resources/image.jpg")
	if err != nil {
		t.Fatalf("cannot open file: %v", err)
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", file.Name())
	if err != nil {
		t.Fatalf("Failed to create a form: %v", err)
	}
	if _, err = io.Copy(part, file); err != nil {
		t.Fatalf("cannot copy file: %v", err)
	}
	if err = writer.Close(); err != nil {
		t.Fatalf("failed on writer close: %v", err)
	}

	reqUpl, err := http.NewRequest("PUT", "http://localhost:8080/upload", body)
	if err != nil {
		t.Fatalf("failed to create a request: %v", err)
	}
	reqUpl.Header.Set("Content-Type", writer.FormDataContentType())
	client := &testutil.SafeClient{
		T: t,
		Client: &http.Client{
			Timeout:   2 * time.Second,
			Transport: &http.Transport{},
		},
	}
	respUpl := client.Do(reqUpl)
	if err != nil {
		t.Fatalf("cannot make request: %v", err)
	}
	defer respUpl.Body.Close()

	if respUpl.StatusCode != http.StatusOK {
		t.Errorf("wrong response code: expected %v, actual %v", respUpl.StatusCode, http.StatusOK)
	}
}

func TestDownloadFile(t *testing.T) {
	req, err := http.NewRequest("GET", "http://localhost:8080/download/image.jpg", nil)
	if err != nil {
		t.Fatalf("failed to create a request: %v", err)
	}
	client := &testutil.SafeClient{
		T: t,
		Client: &http.Client{
			Timeout:   2 * time.Second,
			Transport: &http.Transport{},
		},
	}
	resp := client.Do(req)
	if err != nil {
		t.Fatalf("не удалось выполнить запрос: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("wrong response code: expected %v, actual %v", resp.StatusCode, http.StatusOK)
	}

	resultFileData, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile("./image_result.jpg", resultFileData, 0644)
	if err != nil {
		log.Fatal(err)
	}

	data, err := os.ReadFile("../../internal/resources/image.jpg")
	if err != nil {
		log.Fatal(err)
	}

	if !bytes.Equal(data, resultFileData) {
		log.Fatal("File is corrupted")
	}
}
