
.PHONY: all

all: lint

lint:
	>&2 go install github.com/golangci/golangci-lint/...@latest
	@>&2 echo "Checking code style"
	>&2 golangci-lint run --timeout 10m ./...

