# File service

## How to run
`docker-compose up --build -d`

## How to test
`Run TestUploadFile test from tests/e2e/upload_file_test`

### Result
#### Step 1
В результате запуска (TestUploadFile) e2e теста мы выполним HTTP Request на ``PUT localhost:8080/upload`` ендпоинт и попробуем загрузить файл 
``internal/resources/image.jpg``
данный файл будет разбит на кусочки и отправлен по HTTP на все зарегистрированные сервера (сервера регистрируются при запуске по типу AutoDiscover), 
в результате мы получим:
```
internal/servers -|
    - ip_addr_1 -| 
        - <md5_hash>.part  
    - ip_addr_2 -|   
    - ...       -|   
    - ip_addr_n -|
```
#### Step 2
`Run TestDownloadFile test from tests/e2e/upload_file_test`

В результате запуска (TestDownloadFile) e2e теста мы выполним 
- HTTP Request на ``GET localhost:8080/download/image.jpg`` ендпоинт 
- В результате будет выполнен HTTP request на каждый из файл-серверов, зарегистрированных в редис хранилище ``GET localhost:8080/extract/image.jpg`` 
- Полученные кусочки данных будет распаршены и собраны в результирующий файл, который будет сохранен в ``./tests/e2e/image_result.jpg``


