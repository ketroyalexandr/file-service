FROM golang:1.19

WORKDIR /app

COPY . .
RUN go build -v -o /bin/app ./cmd

CMD ["/bin/app"]
